# -*- coding: utf-8 -*-
import psycopg2
from psycopg2 import pool

from config_reader import TecnoWebConfiguration

class DataBase:
    def session(self):
        return self._driver.getcomm()

    def __repr__(self):
        return 'DB(url=%s)' % self._host

    def __init__(self):
        configuration = TecnoWebConfiguration()
        self.host = configuration.postgres_host
        self.user = configuration.postgres_user
        self.password = configuration.postgres_password
        self.port = configuration.postgres_port

        self.driver = psycopg2.pool.SimpleConnectionPool(1, 20, user=self._user, password=self._password,

                                                         host=self.host, port=self.port, database=self._database)

        if self._driver:
            print("Connection pool create succsessfully")


