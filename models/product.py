# -*- coding: utf-8 -*-
from database import DataBase


class Product:
    def __init__(self, product):
        self._name = product.get('name', False)
        self._code = product.get('code', False)

    @staticmethod
    def browse(product_id):
        browse_product_query = """select * from product where id = {product_id}""".format(product_id=product_id)
        db = DataBase()
        ps_connection = db.session()
        ps_cursor = ps_connection.curor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchnone()
        return product

    @property
    def name(self):
        return self._name

    @property
    def code(self):
        return self._code

    @name.setter
    def name(self, name):
        self._name=name

    @code.setter
    def code(self, code):
        self.code = code

    def __repr__(self):
        return "Product" % self.name
